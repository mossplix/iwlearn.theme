from setuptools import setup, find_packages
import os

version = '1.0'

setup(name='Iwlearn.theme',
      version=version,
      description="",
      long_description=open("README.txt").read() + "\n" +
                       open(os.path.join("docs", "HISTORY.txt")).read(),
      # Get more strings from
      # http://pypi.python.org/pypi?:action=list_classifiers
      classifiers=[
        "Framework :: Karl",
        "Programming Language :: Python",
        ],
      keywords='',
      author='',
      author_email='',
      license='GPL',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['Iwlearn'],
      include_package_data=True,
      zip_safe=False,

      )
